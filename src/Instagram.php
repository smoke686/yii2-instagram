<?php

namespace jekhe\instagram;


use jekhe\instagram\models\Hashtag;
use jekhe\instagram\models\InstagramClient;
use yii\base\Component;

/**
 * Class Instagram
 * @package jekhe\instagram\components
 */
class Instagram extends Component
{
    /** @var string */
    public $username = '';
    /** @var string */
    public $password = '';
    /** @var string|array|null */
    public $proxy = null;
    /** @var bool */
    public $debug = false;
    /** @var array */
    public $config = [];
    /** @var bool */
    public $truncatedDebug = false;
    /** @var bool */
    public $allowDangerousWebUsageAtMyOwnRisk = true;
    /** @var InstagramClient|null */
    private static $_instagramClient = null;
    /** @var string */
    private $_lastError = '';

    /**
     * @return InstagramClient
     */
    public function getClient()
    {
        if (!static::$_instagramClient) {
            InstagramClient::$allowDangerousWebUsageAtMyOwnRisk = $this->allowDangerousWebUsageAtMyOwnRisk;
            $instagramClient = new InstagramClient($this->debug, $this->truncatedDebug, $this->config);
            if($this->proxy){
                $instagramClient->setProxy($this->proxy);
            }
            static::$_instagramClient = $instagramClient;
        }
        return static::$_instagramClient;
    }

    /**
     * @param string $message
     */
    private function setLastError($message)
    {
        $this->_lastError = $message;
    }

    /**
     * Авторизация
     *
     * @return bool
     */
    public function login()
    {
        $ig = $this->getClient();
        $login = null;
        try {
            $login = $ig->login($this->username, $this->password);
        } catch (\Exception $e) {
            $this->setLastError($e->getMessage());
        }
        return isset($login) || (!isset($login) && $ig->isMaybeLoggedIn);
    }

    /**
     * Получение постов по хештегу
     *
     * @param string $hashtag
     * @return \InstagramAPI\Response\TagFeedResponse
     */
    public function findByHashtag($hashtag)
    {
        $hashTag = new Hashtag($this->getClient());
        return $hashTag->getFeed($hashtag);
    }
}
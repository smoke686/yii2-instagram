<?php

namespace jekhe\instagram\models;


use InstagramAPI\Signatures;

/**
 * Class Hashtag
 * Переопределенные методы для лучшего взаимодействия с Yii2
 *
 * @package jekhe\instagram\models
 */
class Hashtag extends \InstagramAPI\Request\Hashtag
{
    /**
     * @return string
     */
    public function generateUuid()
    {
        return Signatures::generateUUID();
    }

    /**
     * @param string $hashtag
     * @param string|null $rankToken
     * @param string|null $maxId
     * @return \InstagramAPI\Response\TagFeedResponse
     */
    public function getFeed(
        $hashtag,
        $rankToken = null,
        $maxId = null)
    {
        return parent::getFeed($hashtag, $rankToken ?? $this->generateUuid(), $maxId);
    }
}
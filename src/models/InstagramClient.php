<?php

namespace jekhe\instagram\models;


use InstagramAPI\Instagram;

/**
 * Class InstagramClient
 * Переопределенные методы для лучшего взаимодействия с Yii2
 *
 * @package jekhe\instagram\models
 */
class InstagramClient extends Instagram
{
    public function __construct(bool $debug = false, bool $truncatedDebug = false, array $storageConfig = [])
    {
        /**
         * Вызов дефолтной конфигурации, если не указано иное
         *
         * @var array $storageConfig
         */
        if (empty($storageConfig)) {
            $storageConfig = [
                'storage' => 'file',
                'basefolder' => \Yii::getAlias('@runtime/sessions/')
            ];
        }
        parent::__construct($debug, $truncatedDebug, $storageConfig);
    }
}